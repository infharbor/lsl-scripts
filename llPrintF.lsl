//################################## RESIZE TO THIS LENGTH ############################### //
//#     ____        ____ _         _  __           __  __              __                
//#    /  _/____   / __/(_)____   (_)/ /_ ___     / / / /____ _ _____ / /_   ____   _____
//#    / / / __ \ / /_ / // __ \ / // __// _ \   / /_/ // __ `// ___// __ \ / __ \ / ___/
//#  _/ / / / / // __// // / / // // /_ /  __/  / __  // /_/ // /   / /_/ // /_/ // /    
//# /___//_/ /_//_/  /_//_/ /_//_/ \__/ \___/  /_/ /_/ \__,_//_/   /_.___/ \____//_/     
//#                                                                                     
//# Script Name     : llPrintF
//# Creater Name    : Co Starsmith
//# Script License  : MIT (c) 2017 Infinite Harbor
//# Versions Number : v1.0.0-RELEASE
//#
//################################## RESIZE TO THIS LENGTH ############################### //

string llPrintF( string str, list parts ) {
    integer len = llGetListLength(parts);
    if( len > 0 ) {
        integer i = 0; integer h = 0;
        list d = llParseString2List(str, [], ["%s"]);
        for ( i = 0; i < llGetListLength(d); i++ ) {
            if(llList2String(d, i) == "%s") {
                string part = llList2String(parts, h);
                if(part == "") part = "%s";
                d = llListReplaceList(d, [part], i, i);
                h++;
            }
        }
        str = llDumpList2String(d, "");
    }
    return str;
}

default {

    state_entry() {

        llOwnerSay(llPrintF("%s, this %s can split up %s and %s.", [
                                        llKey2Name(llGetOwner()),
                                        "script", "strings", "their variables"
                                    ]));
    }
}
